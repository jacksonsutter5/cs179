from canvasapi import Canvas
from dotenv import load_dotenv, dotenv_values

CANVAS_API_URL = 'https://sdccd.instructure.com'
CANVAS_COURSE_ID = 2450635
CANVAS_COURSE_URL = f'https://sdccd.instructure.com/courses/{CANVAS_COURSE_ID}'

# Initialize a new Canvas object
load_dotenv()
ENV = dotenv_values()


canvas = Canvas(CANVAS_API_URL, ENV['CANVAS_API_KEY'])
print(canvas)
