# from colorama import init
import canvas_grab
from pathlib import Path
# from termcolor import colored
from canvasapi.exceptions import ResourceDoesNotExist

from canvasapi import Canvas
from dotenv import load_dotenv, dotenv_values
import pandas as pd

CANVAS_API_URL = 'https://sdccd.instructure.com'
CANVAS_COURSE_ID = 2450635
CANVAS_COURSE_URL = f'https://sdccd.instructure.com/courses/{CANVAS_COURSE_ID}'

# Initialize a new Canvas object
load_dotenv()
ENV = dotenv_values()

canvas = Canvas(CANVAS_API_URL, ENV['CANVAS_API_KEY'])
print(canvas)


# interactive, noupdate, config = .get_options()
# canvas = config.endpoint.login()
courses = list(canvas.get_courses())
print(courses)

course_id = next(iter([c.id for c in courses if 'CISC 179' in c.name]))
print(course_id)

course = canvas.get_course(course_id)
grade_changes = course.get_grade_change_events()
print(grade_changes)

sections = list(course.get_sections())
assert len(sections) == 1
section = sections[0]
print(section)

student_summaries = list(course.get_course_level_student_summary_data())
print(student_summaries[0])

enrollments = list(section.get_enrollments())

enrollmentenrollments[0]
'@' in str(enrollments[0])
'@' in str(enrollments[0]['user'])
enrollments[0].user
enrollments[1].user
df_users = pd.DataFrame([enr.user for enr in enrollments])
df_users = pd.DataFrame([enr.user for enr in enrollments])
df_users
df_num_interacts = pd.read_csv(
    'data/private/2023-02-24-wk3-number-of-interactions-data_for_sdccd_mesa_college_cs179_spring23.csv', index_col=0, header=0)
df_num_interacts = pd.read_csv(
    '~/code/teach/cs179/data/private/2023-02-24-wk3-number-of-interactions-data_for_sdccd_mesa_college_cs179_spring23.csv', index_col=0, header=0)
df_activity_log = pd.read_csv(
    '/home/hobs/code/teach/cs179/data/private/2023-02-24-wk3-data_for_sdccd_mesa_college_cs179_spring23.csv', index_col=0, header=0)
df_activity_log = df_activity_log.T
df_num_interacts = df_num_interacts.T
df_num_interacts.index
df_num_interacts.sum(axis=1)
df_num_interacts.str.len() > 0
df_num_interacts
df_num_interacts.iloc[1:]
df_num_interacts.iloc[2:]
df_num_interacts.iloc[2:].astype(float)
dfnums = df_num_interacts.iloc[2:]
dfnums.str.len()
dfnums[dfnums.columns].str.len()
[dfnums[c].str.len() for c in dfnums.columns]
pd.DataFrame([dfnums[c].str.strip().str.len() == 0 for c in dfnums.columns], columns=dfnums.columns, index=dfnums.index)
pd.DataFrame(pd.DataFrame([dfnums[c].str.strip().str.len() == 0 for c in dfnums.columns]).T, columns=dfnums.columns, index=dfnums.index)
dfnums = pd.DataFrame(pd.DataFrame([dfnums[c].str.strip().str.len() == 0 for c in dfnums.columns]).T, columns=dfnums.columns, index=dfnums.index)
dfnums0 = dfnums
dfnums = df_num_interacts.iloc[2:]
dfnums0 = pd.DataFrame(pd.DataFrame([dfnums[c].str.strip().str.len() == 0 for c in dfnums.columns]).T, columns=dfnums.columns, index=dfnums.index)
dfnums[dfnums0.values] = 0
dfnums.fillna(0)
dfnums = dfnums.fillna(0)
dfnums.astype(float)
dfnums = dfnums.astype(float)
[s.split('<br>') for s in dfnums.index.values]
[s.split('<br>')[0] for s in dfnums.index.values]
[s.split('<br>')[0].split(',') for s in dfnums.index.values]
dfnums.index = [s.split('<br>')[0].lower().split(',') for s in dfnums.index.values]
dfnums.index = [tuple(s.split('<br>')[0].lower().split(',')) for s in dfnums.index.values]
dfnums
df_users
dfnums = pd.DataFrame(pd.DataFrame([dfnums[c].str.strip().str.len() == 0 for c in df_num_interacts.iloc[2:].columns]
                      ).T, columns=dfnums.columns, index=dfnums.index)
dfnums = df_num_interacts.iloc[2:]
dfnums = pd.DataFrame(pd.DataFrame([dfnums[c].str.strip().str.len() == 0 for c in dfnums.columns]).T, columns=dfnums.columns, index=dfnums.index)

df_nums = df_num_interacts.iloc[2:].astype(float)
dfnums = dfnums.fillna(0)U
dfnums.index = [tuple(s.split('<br>')[0].lower().split(',') + s.split('<br>')[-1][1:-1]) for s in dfnums.index.values]
dfnums.index = [tuple(s.split('<br>')[0].lower().split(',') + [s.split('<br>')[-1][1:-1]]) for s in dfnums.index.values]
dfnums.index
dfnums.index.values
pd.DataFrame(dfnums.index.values)
pd.DataFrame(list(x) for x in dfnums.index.values])
pd.DataFrame([list(x) for x in dfnums.index.values])
df_runstone_roster= pd.DataFrame([list(x) for x in dfnums.index.values])
df_runestone_roster= pd.DataFrame([list(x) for x in dfnums.index.values])
df_runestone_roster.columns= ['last', 'first', 'username']
dfnums.index.values
df_num_interacts.iloc[2: ].astype(float)
df_users
df_users['last']= [df_users['name'].str.split()[-1]]
df_users[['first', 'last']]= df_users['name'].str.split()
df_users['name'].str.split()
df_users['last']= [n[-1] for n in df_users['name'].str.split()]
df_users[['name', 'last']]
[n for n in dfnums['last'] if n in df_users['last'].str.lower()]
dfnums.columns
dfnums
[n[1] for n in dfnums.index if n in df_users['last'].str.lower()]
[n[0] for n in dfnums.index if n in df_users['last'].str.lower()]
[n for n in df_users['last'].str.lower()]
[n[1] for n in dfnums.index]
[n[0] for n in dfnums.index]
[n[0] for n in dfnums.index if n[0] in df_users['last'].str.lower()]
[n[0] for n in dfnums.index if n[0] in df_users['last'].str.lower().str.strip()]
set(df_users['last'].str.lower().str.strip())
userlastset= set(df_users['last'].str.lower().str.strip())
runelastset= set(n[0] for n in dfnums.index)
runelastset.intersection(userlastset)
userlastlist= list(df_users['last'].str.lower().str.strip())
df_users['last']= userlastlist
df_run= dfnums
df_rune= dfnums
df_rune
df_rune['last']= [i[0] for i in df_rune.index]
df_rune['last']= [i[0] for i in list(df_rune.index.values)]
df_rune['last']
df_rune.join(df_users, on='last', how='outer')
df_rune.join(df_users, on='last', how='inner')
df_rune.index
df_rune['fullname']= df_rune.index.values
df_rune['first']= [x[1] for x in df_rune.index.values]
df_rune['runeuser']= [x[-1] for x in df_rune.index.values]
df_rune
df.reset_index()
df_rune.reset_index()
df_rune.reset_index(drop=True)
df_rune= df_rune.reset_index(drop=True)
df_users
df_users.join?
df_users.join(df_rune, on='last')
df_rune.columns= [str(x).strip() for x in df_rune.columns]
df_users.columns= [str(x).strip() for x in df_users.columns]
df_users.join(df_rune, on='last')
df_rune.set_index('last')
df_rune= df_rune.set_index('last')
df_users= df_rune.set_index('last')
df_users
df_users= df_users.set_index('last')
pd.concat([df_users, df_rune], axis=1)
len(df_users.index)
len(set(df_users.index))
sorted(df_users.index)
df_users.loc['student']
idx= [s + f'{i}' if s == 'student' else s for i, s in enumerate(df_users.index)]
np.array(idx) == df_users.index.values
import numpy as np
np.array(idx) == df_users.index.values
idx
df_users.index= idx
df_users
pd.concat([df_users, df_rune], axis=1)
df_canvas_rune_join= pd.concat([df_users, df_rune], axis=1)
df_canvas_rune_join.to_csv('df_users_canvas_runestone_joined.csv')
ls
hist - o - p - f 'df_users_canvas_runestone_joined.csv.hist.ipy'
hist - f 'df_users_canvas_runestone_joined.csv.hist.py'
ls
hist - f df_users_canvas_runestone_joined.csv.hist.py
hist - o - p - f df_users_canvas_runestone_joined.csv.hist.ipy
ls
rm 'df_users_canvas_runestone_joined.csv'
ls
df_canvas_rune_join.to_csv('df_users_canvas_runestone_joined.csv')
rm "'df_users_canvas_runestone_joined.csv.hist.*'"
rm "'df_users_canvas_runestone_joined.csv.hist.ipy'"
rm "'df_users_canvas_runestone_joined.csv.hist.py'"
ls
mv df_users_ * .. / cs179 / data / private/
